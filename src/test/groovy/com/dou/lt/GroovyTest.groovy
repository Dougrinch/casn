package com.dou.lt

import groovy.transform.CompileStatic
import org.junit.jupiter.api.Test

@CompileStatic
class GroovyTest {

    static String getName() {
        return "Groovy";
    }

    @Test
    void groovy() {
        System.out.println("groovy + " + JavaTest.getName())
        System.out.println("groovy + " + GroovyTest.getName())
    }
}
