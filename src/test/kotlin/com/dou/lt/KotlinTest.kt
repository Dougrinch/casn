package com.dou.lt

import org.junit.jupiter.api.Test

class KotlinTest {

    companion object {
        @JvmStatic
        val name: String
            get() {
                return "Kotlin"
            }
    }

    @Test
    fun kotlin() {
        println("kotlin + " + JavaTest.getName())
        println("kotlin + " + GroovyTest.getName())
        println("kotlin + " + KotlinTest.name)
    }
}
