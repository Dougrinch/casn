package com.dou.lt

import java.util.concurrent.atomic.AtomicReference
import java.util.concurrent.atomic.AtomicReferenceArray

fun main() {
    val q = ArrayQueue<String>(3)

    q.add("1")
    q.add("2")
    println(q.remove())
    q.add("3")
    println(q.remove())
    q.add("4")
    q.add("5")

    while (q.size() > 0) {
        println(q.remove())
    }
}

class ArrayQueue<T>(size: Int) {
    private val data = AtomicReferenceArray<Any?>(size)
    private var size = AtomicReference<Any?>(0).asPtr(-1)

    fun size(): Int {
        return CASN.read(size) as Int
    }

    fun add(t: T) {
        while (true) {
            val idx = size()
            if (CASN.casn(CASN.UpdateEntry(size, idx, idx + 1),
                            CASN.UpdateEntry(ptrTo(idx), null, t))) {
                return
            }
        }
    }

    @Suppress("UNCHECKED_CAST")
    fun remove(): T {
        while (true) {
            val idx = size()
            val valueRef = ptrTo(idx - 1)
            val value = CASN.read(valueRef)
            if (CASN.casn(CASN.UpdateEntry(size, idx, idx - 1),
                            CASN.UpdateEntry(valueRef, value, null))) {
                return value as T
            }
        }
    }

    private fun ptrTo(i: Int): UnsafeOrderedPtr {
        return PtrToIdx(i)
    }

    private inner class PtrToIdx(override val position: Int) : UnsafeOrderedPtr {
        override fun read(): Any? {
            return data.get(position)
        }

        override fun cas(expectedValue: Any?, newValue: Any?): Any? {
            while (true) {
                val prev = data.get(position)
                if (data.compareAndSet(position, expectedValue, newValue)) {
                    return prev
                }
            }
        }
    }
}
