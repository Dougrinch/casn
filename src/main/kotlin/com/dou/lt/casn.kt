package com.dou.lt

import com.dou.lt.CASN.Status.*
import java.util.concurrent.atomic.AtomicReference

interface SafePtr<T> {
    fun read(): T
    fun cas(expectedValue: T, newValue: T): T
}

interface SafeOrderedPtr<T> : SafePtr<T> {
    val position: Int
}

typealias UnsafePtr = SafePtr<Any?>
typealias UnsafeOrderedPtr = SafeOrderedPtr<Any?>

object CASN {
    private enum class Status {
        UNDECIDED, FAILED, SUCCEEDED
    }

    class UpdateEntry(val valueRef: UnsafeOrderedPtr, val expectedValue: Any?, val newValue: Any?)
    private class Descriptor(status: Status, val entries: List<UpdateEntry>) {
        val statusRef = AtomicReference(status).asPtr()
    }

    fun read(valueRef: UnsafePtr): Any? {
        while (true) {
            when (val r = rdcssRead(valueRef)) {
                is Descriptor -> casn(r)
                else -> return r
            }
        }
    }

    fun casn(vararg updates: UpdateEntry): Boolean {
        return casn(Descriptor(UNDECIDED, updates.copyOf().sortedBy { it.valueRef.position }))
    }

    private fun casn(desc: Descriptor): Boolean {
        if (desc.statusRef.read() == UNDECIDED) {
            var status = SUCCEEDED
            for (entry in desc.entries) {
                do {
                    val entryStatus = setEntryDescriptor(desc, entry)
                    if (entryStatus == FAILED) {
                        status = FAILED
                        break
                    }
                } while (entryStatus == UNDECIDED)
            }

            desc.statusRef.cas(UNDECIDED, status)
        }

        val succeeded = desc.statusRef.read() == SUCCEEDED
        for (entry in desc.entries) {
            entry.valueRef.cas(desc, if (succeeded) entry.newValue else entry.expectedValue)
        }
        return succeeded
    }

    private fun setEntryDescriptor(desc: Descriptor, entry: UpdateEntry): Status {
        return when (val r = rdcss(RdcssDesc(desc.statusRef, UNDECIDED, entry.valueRef, entry.expectedValue, desc))) {
            is Descriptor -> {
                if (r != desc) {
                    casn(r)
                    UNDECIDED
                } else {
                    SUCCEEDED
                }
            }
            else -> {
                if (r != entry.expectedValue) {
                    FAILED
                } else {
                    SUCCEEDED
                }
            }
        }
    }

    private class RdcssDesc<F>(val flagRef: SafePtr<F>, val expectedFlag: F,
                               val valueRef: UnsafePtr, val expectedValue: Any?, val newValue: Any?)

    private fun rdcssRead(valueRef: UnsafePtr): Any? {
        while (true) {
            when (val r = valueRef.read()) {
                is RdcssDesc<*> -> complete(r)
                else -> return r
            }
        }
    }

    private fun rdcss(desc: RdcssDesc<*>): Any? {
        while (true) {
            when (val r = desc.valueRef.cas(desc.expectedValue, desc)) {
                is RdcssDesc<*> -> complete(r)
                else -> {
                    if (r == desc.expectedValue) {
                        complete(desc)
                    }
                    return r
                }
            }
        }
    }

    private fun <T> complete(desc: RdcssDesc<T>) {
        if (desc.flagRef.read() == desc.expectedFlag) {
            desc.valueRef.cas(desc, desc.newValue)
        } else {
            desc.valueRef.cas(desc, desc.expectedValue)
        }
    }
}

fun <T> AtomicReference<T>.asPtr(): SafePtr<T> = asPtr(Int.MIN_VALUE)

fun <T> AtomicReference<T>.asPtr(position: Int): SafeOrderedPtr<T> {
    return object : SafeOrderedPtr<T> {
        override val position: Int
            get() = position

        override fun read(): T {
            return this@asPtr.get()
        }

        override fun cas(expectedValue: T, newValue: T): T {
            while (true) {
                val prev = this@asPtr.get()
                if (this@asPtr.compareAndSet(expectedValue, newValue)) {
                    return prev
                }
            }
        }
    }
}
